## Font Generator using TotalCross

This project has some fonts used to generate TotalCross font file for Pink T-rano app.

All the fonts support at least Korean characters.


## Example generating new fonts

Run the following command inside project folder.

Using **.jar**

```
Korean

java -cp ./totalcross-sdk.jar tc.tools.FontGenerator "Malgun Gothic" /sizes:10,20,40 /aa /u 32-255 256-383 402-402 506-511 8352-8399 4352-4607 12288-12291 12296-12309 12592-12687 19968-25343 25344-30719 30720-36095 36096-40959 43360-43391 44032-45055 45056-49151 49152-53247 53248-55215 55216-55295 65280-65519 
```


Generates font holding Chinese, Japanese and Korean characters.
(Only working with Arial Unicode MS)
```
Chinese, Japanese and Korean

java -cp ./totalcross-sdk.jar tc.tools.FontGenerator "Arial Unicode MS" /sizes:10,20,40 /aa /u 32-255 256-383 402-402 506-511 8352-8399 4352-4607 12288-12291 12296-12309 12353-12447 12448-12543 12592-12687 12688-12703 19968-25343 25344-30719 30720-36095 36096-40959 43360-43391 44032-45055 45056-49151 49152-53247 53248-55215 55216-55295 65280-65519 
```

## Fonts

The folder **FONTS** has some True Type Font files that are able to generate the desired characters.
To use them, make sure they are installed on the computer before running the commands.

Some of the fonts will not work if they don't support every character nedded.
It is possible to check so using [BabelStone](http://www.babelstone.co.uk/Software/BabelMap.html) (only Windows)




